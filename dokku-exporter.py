from pathlib import Path
from http.server import BaseHTTPRequestHandler, HTTPServer
import json


class DokkuHostnameExporter(BaseHTTPRequestHandler):
    DOKKU_HOME = Path("~dokku").expanduser().resolve()
    VHOST_FILE_NAME = "VHOST"

    def version_string(self) -> str:
        return "Magic"

    def do_GET(self):
        lines = []

        for app_dir in self.DOKKU_HOME.iterdir():
            if not app_dir.is_dir():
                continue

            host_file = app_dir / self.VHOST_FILE_NAME

            try:
                if not host_file.is_file():
                    continue
            except PermissionError:
                continue

            vhosts = host_file.read_text().splitlines()

            if not vhosts:
                continue

            lines.append({
                "app": app_dir.name,
                "vhosts": vhosts
            })

        data = json.dumps(lines)

        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(data.encode())

def main():
    web_server = HTTPServer(("0.0.0.0", 8000), DokkuHostnameExporter)

    try:
        web_server.serve_forever()
    except KeyboardInterrupt:
        pass

    web_server.server_close()
    print("Server stopped.")


if __name__ == "__main__":
    main()
